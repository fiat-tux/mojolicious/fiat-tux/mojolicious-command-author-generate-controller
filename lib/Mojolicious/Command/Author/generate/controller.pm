# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package Mojolicious::Command::Author::generate::controller;
use Mojo::Base 'Mojolicious::Command', -signatures;
use Mojo::Util qw(class_to_file class_to_path decamelize);

has description => 'Generate Mojolicious Fiat Tux controller files';
has usage       => sub { shift->extract_usage };

our $VERSION = "0.02";

sub run ($self, $class = 'MyApp', @controllers) {

    # Model
    @controllers = qw/user/ unless scalar @controllers;
    for my $controller (@controllers) {
        $controller    = ucfirst($controller);
        my $controller = "${class}::Controller::$controller";
        my $path       = class_to_path $controller;
        $self->render_to_rel_file('controller', "lib/$path", { class => $controller, base_class => $class, controller => lc($controller).'s' });
    }
}

1;

=pod

=encoding utf-8

=head1 NAME

Mojolicious::Command::Author::generate::controller - Fiat Tux controller generator command

=head1 SYNOPSIS

  Usage: APPLICATION generate controller [OPTIONS] [NAME]
    mojo generate controller
    mojo generate controller TestApp
    mojo generate controller TestApp user article comment
  Options:
    -h, --help   Show this summary of available options
  Nota bene:
    run it from your application directory

=head1 DESCRIPTION

L<Mojolicious::Command::Author::generate::controller> generates controller files for L<Mojolicious> applications, with Fiat Tux flavor.

Forked from L<Mojolicious::Command::Author::generate::app>
See L<Mojolicious::Commands/"COMMANDS"> for a list of commands that are available by default.

=head1 ATTRIBUTES

L<Mojolicious::Command::Author::generate::controller> inherits all attributes from L<Mojolicious::Command> and implements the
following new ones.

=head2 description

  my $description = $app->description;
  $app            = $app->description('Foo');

Short description of this command, used for the command list.

=head2 usage

  my $usage = $app->usage;
  $app      = $app->usage('Foo');

Usage information for this command, used for the help screen.

=head1 METHODS

L<Mojolicious::Command::Author::generate::controller> inherits all methods from L<Mojolicious::Command> and implements the
following new ones.

=head2 run

  $app->run(@ARGV);

Run this command.

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Guides>, L<https://mojolicious.org>.

=head1 AUTHOR

Luc Didry E<lt>luc@didry.orgE<gt>

=head1 LICENSE

Copyright (C) Luc Didry.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__DATA__

@@ controller
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base 'Mojolicious::Controller', -signatures;



1;
